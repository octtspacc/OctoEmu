var LoadedCore;
var TouchKeyEvDict = {
	"touchstart": "keydown",
	"touchend":   "keyup",
	"mousedown":  "keydown",
	"mouseup":    "keyup",
	"mouseout":   "keyup",
};
var DirStickModeDict = { "Vir-Abs": "dynamic", "Vir-Rel": "static" };
var WordedToSymbolicButtons = { left: "⬅️", up: "⬆️", right: "➡️", down: "⬇️" };

// DEVNOTE: Use <https://www.toptal.com/developers/keycode> to discover codes
var KeyCodesMap = {
	Current: {},
	Default: {
		A:      99, // Numpad3
		B:      98, // Numpad2
		Start:  13, // Enter
		Select: 16, // ShiftRight
		"⬅️":   37, // ArrowLeft
		"⬆️":   38, // ArrowUp
		"➡️":   39, // ArrowRight
		"⬇️":   40, // ArrowDown
	},
};

var TouchKeysMaps = {
	Current: {},
	Default: {
		A:      {Class: "Btn-Circ  Vir-Abs", Style: "bottom: 0px; right: 0px;"},
		B:      {Class: "Btn-Circ  Vir-Abs", Style: "bottom: 0px; right: calc(var(--Btn-Circ-Size) + 8px);"},
		Y:      {Class: "Btn-Circ  Vir-Abs", Style: "bottom: calc(var(--Btn-Circ-Size) + 8px); right: 0px;"},
		X:      {Class: "Btn-Circ  Vir-Abs", Style: "bottom: calc(var(--Btn-Circ-Size) + 8px); right: calc(var(--Btn-Circ-Size) + 8px);"},
		L:      {Class: "Btn-Rect  Vir-Abs", Style: "bottom: 50%; left: 0px;"},
		R:      {Class: "Btn-Rect  Vir-Abs", Style: "bottom: 50%; right: 0px;"},
		Start:  {Class: "Btn-Rect  Vir-Abs", Style: "bottom: 0px; right: 60%;"},
		Select: {Class: "Btn-Rect  Vir-Abs", Style: "bottom: 0px; right: 40%;"},
		Dir0:   {Class: "Dir-Stick Vir-Rel", Style: "bottom: 50px; left: 50px; width: 150px; height: 150px;"},
		/*
		"⬅️": "bottom: 24px; left: 0px;",
		"⬆️": "bottom: 48px; left: 24px;",
		"➡️": "bottom: 24px; left: 48px;",
		"⬇️": "bottom: 0px; left: 24px;",
		*/
	},
	NES: {
		A:      null,
		B:      null,
		Start:  null,
		Select: null,
		Dir0:   null,
		/*
		"⬅️": null,
		"⬆️": null,
		"➡️": null,
		"⬇️": null,
		*/
	},
};

function SetupKeyCodes() {
	[KeyCodesMap.Default, KeyCodesMap[LoadedCore] || {}].forEach((CodesMap) => {
		Object.keys(CodesMap).forEach((Code) => {
			KeyCodesMap.Current[Code] = KeyCodesMap.Default[Code];
		});
	});
};

function SetupKeyHandler() {
	Object.keys(TouchKeysMaps[LoadedCore]).forEach((Key) => {
		var Ctl = document.createElement('span');
		var CtlData = TouchKeysMaps[LoadedCore][Key] || TouchKeysMaps['Default'][Key];
		Ctl.className = 'VirCtl ' + CtlData.Class;
		Ctl.dataset.virctl = Key;
		Ctl.textContent = Key;
		Ctl.style = CtlData.Style;
		VirCtlDiv.append(Ctl);
		var Classes = CtlData.Class.split(' ');
		if (Classes.includes('Dir-Stick')) {
			Ctl = nipplejs.create({zone: Ctl, mode: DirStickModeDict[Classes.slice(-1)], position: {left: "50%", top: "50%"}});
			Ctl.on('start end', function (evt, data) {
				console.log(evt, data);
			}).on('move', function (evt, data) {
				//console.log(evt, data);
			}).on('dir:up plain:up dir:left plain:left dir:down plain:down dir:right plain:right', function (evt, data) {
				console.log(evt, data);
				if (data.direction) { // TODO... we should read joy position, store it somewhere, and send keys in the start/end/move events
					SendPlayerKeyEvent(KeyCodesMap.Current[WordedToSymbolicButtons[evt.type.split(':')[1]]], 'keydown');
				}
			}).on('pressure', function (evt, data) {
				//console.log(evt, data);
			});
		};
		//console.log(Ctl);
	});
	['mousedown', 'mouseup', 'mouseout', 'touchstart', 'touchmove', 'touchend' /*, 'touchcancel', 'touchenter', 'touchleave'*/].forEach((Ev) => {
		window.addEventListener(Ev, HandleTouch);
	});

	window.onkeydown = function(Ev) { SendPlayerKeyEvent(Ev.keyCode, Ev.type); };
	window.onkeyup = function(Ev) { SendPlayerKeyEvent(Ev.keyCode, Ev.type); };
};

function HandleTouch(Ev) {
	//console.log(Ev);
	//document.querySelector('.VirKeyBtn[data-key=""]')
	console.log(/*Ev,*/ Ev.type, /*Ev.target,*/ Ev.target.dataset.virctl);
	SendPlayerKeyEvent(KeyCodesMap.Current[Ev.target.dataset.virctl], TouchKeyEvDict[Ev.type]);
};

// <https://gist.github.com/GlauberF/d8278ce3aa592389e6e3d4e758e6a0c2>
function SendPlayerKeyEvent(Code, Type, Modifiers) {
	//var EvName = (typeof(Type) === 'string') ? 'key' + Type : 'keydown';
	var EvName = (typeof(Type) === 'string') ? Type : 'keydown';
	var Modifier = (typeof(Modifiers) === 'object') ? Modifier : {};
	var Ev = PlayerFrame.contentWindow.document.createEvent('HTMLEvents');
	Ev.initEvent(EvName, true, false);
	Ev.keyCode = Code;
	for (var i in Modifiers) {
		Ev[i] = Modifier[i];
	};
	PlayerFrame.contentWindow.document.dispatchEvent(Ev);
};

//document.addEventListener('touchmove', function(event) {
//	event.preventDefault();
//});

LoadedCore = 'Default';
SetupKeyCodes();
SetupKeyHandler();

// NOTE: We need to use srcdoc if we're not on a server, otherwise we have CORS access issues!
PlayerFrame.srcdoc = `
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>Dummy Test App</title>
	</head>
	<body style="overflow: hidden;">
		<div id="s" style="background: white; position: absolute; width: 50px; height: 100px;"></div>
		<script>
			var Pressed = { e: false, l: false, r: false, u: false, d: false };
			s.style.top = '50px';
			s.style.left = '50px';
			function Cycle() {
				if (Pressed.e) alert(window.location);
				if (Pressed.l) s.style.left = parseInt(s.style.left.split('px')[0]) - 1 + 'px';
				if (Pressed.r) s.style.left = parseInt(s.style.left.split('px')[0]) + 1 + 'px';
				if (Pressed.u) s.style.top = parseInt(s.style.top.split('px')[0]) - 1 + 'px';
				if (Pressed.d) s.style.top = parseInt(s.style.top.split('px')[0]) + 1 + 'px';
				//console.log(Pressed);
			};
			window.onkeydown = function(e) {
				if (e.keyCode == 13) {
					//Pressed.e = true;
					alert(window.location);
				}
				if (e.keyCode == 98 || e.keyCode == 37) Pressed.l = true;
				if (e.keyCode == 99 || e.keyCode == 39) Pressed.r = true;
				if (e.keyCode == 38) Pressed.u = true;
				if (e.keyCode == 40) Pressed.d = true;
				console.log(e.keyCode);
			};
			window.onkeyup = function(e) {
				if (e.keyCode == 13) {
					Pressed.e = false;
					//alert(window.location);
				}
				if (e.keyCode == 98 || e.keyCode == 37) Pressed.l = false;
				if (e.keyCode == 99 || e.keyCode == 39) Pressed.r = false;
				if (e.keyCode == 38) Pressed.u = false;
				if (e.keyCode == 40) Pressed.d = false;
			};
			setInterval(Cycle, 1/60);
		</script>
	</body>
</html>
`;
